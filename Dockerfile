
FROM voidlinux/voidlinux
MAINTAINER Toyam Cox <Vaelatern@gmail.com>

RUN xbps-install -Sy
RUN xbps-install -yu
RUN xbps-install -yu
RUN xbps-install -yu
RUN xbps-install -y base-devel zeromq-devel czmq-devel libxbps-devel sqlite-devel libbsd-devel libgit2-devel graphviz-devel pkg-config clang git check-devel gsl-ucg
